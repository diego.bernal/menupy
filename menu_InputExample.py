import menupy

#InputMenu con sus metodos es para escribir datos y te devuelve un json
NewMenu = menupy.InputMenu("Title", title_color="cyan")
NewMenu.add_input("Input #1")
NewMenu.add_input("Input #2", color="magenta")
NewMenu.add_input("Input #3", color="yellow", input_text="default", input_color="blue")
NewMenu.add_input("Input #4", input_text='dale enter y borrame', color='black', bg_color='blue', input_color='magenta', input_bg_color='yellow', input_length=20)
result = NewMenu.run()

print(result)
